package hm.web.rest;

import hm.HmApp;

import hm.domain.Scores;
import hm.repository.ScoresRepository;
import hm.service.ScoresService;
import hm.service.dto.ScoresDTO;
import hm.service.mapper.ScoresMapper;
import hm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static hm.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ScoresResource REST controller.
 *
 * @see ScoresResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HmApp.class)
public class ScoresResourceIntTest {

    private static final Integer DEFAULT_SCORE = 1;
    private static final Integer UPDATED_SCORE = 2;

    @Autowired
    private ScoresRepository scoresRepository;

    @Autowired
    private ScoresMapper scoresMapper;

    @Autowired
    private ScoresService scoresService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restScoresMockMvc;

    private Scores scores;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ScoresResource scoresResource = new ScoresResource(scoresService);
        this.restScoresMockMvc = MockMvcBuilders.standaloneSetup(scoresResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Scores createEntity(EntityManager em) {
        Scores scores = new Scores()
            .score(DEFAULT_SCORE);
        return scores;
    }

    @Before
    public void initTest() {
        scores = createEntity(em);
    }

    @Test
    @Transactional
    public void createScores() throws Exception {
        int databaseSizeBeforeCreate = scoresRepository.findAll().size();

        // Create the Scores
        ScoresDTO scoresDTO = scoresMapper.toDto(scores);
        restScoresMockMvc.perform(post("/api/scores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scoresDTO)))
            .andExpect(status().isCreated());

        // Validate the Scores in the database
        List<Scores> scoresList = scoresRepository.findAll();
        assertThat(scoresList).hasSize(databaseSizeBeforeCreate + 1);
        Scores testScores = scoresList.get(scoresList.size() - 1);
        assertThat(testScores.getScore()).isEqualTo(DEFAULT_SCORE);
    }

    @Test
    @Transactional
    public void createScoresWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = scoresRepository.findAll().size();

        // Create the Scores with an existing ID
        scores.setId(1L);
        ScoresDTO scoresDTO = scoresMapper.toDto(scores);

        // An entity with an existing ID cannot be created, so this API call must fail
        restScoresMockMvc.perform(post("/api/scores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scoresDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Scores in the database
        List<Scores> scoresList = scoresRepository.findAll();
        assertThat(scoresList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllScores() throws Exception {
        // Initialize the database
        scoresRepository.saveAndFlush(scores);

        // Get all the scoresList
        restScoresMockMvc.perform(get("/api/scores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(scores.getId().intValue())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)));
    }
    
    @Test
    @Transactional
    public void getScores() throws Exception {
        // Initialize the database
        scoresRepository.saveAndFlush(scores);

        // Get the scores
        restScoresMockMvc.perform(get("/api/scores/{id}", scores.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(scores.getId().intValue()))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE));
    }

    @Test
    @Transactional
    public void getNonExistingScores() throws Exception {
        // Get the scores
        restScoresMockMvc.perform(get("/api/scores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateScores() throws Exception {
        // Initialize the database
        scoresRepository.saveAndFlush(scores);

        int databaseSizeBeforeUpdate = scoresRepository.findAll().size();

        // Update the scores
        Scores updatedScores = scoresRepository.findById(scores.getId()).get();
        // Disconnect from session so that the updates on updatedScores are not directly saved in db
        em.detach(updatedScores);
        updatedScores
            .score(UPDATED_SCORE);
        ScoresDTO scoresDTO = scoresMapper.toDto(updatedScores);

        restScoresMockMvc.perform(put("/api/scores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scoresDTO)))
            .andExpect(status().isOk());

        // Validate the Scores in the database
        List<Scores> scoresList = scoresRepository.findAll();
        assertThat(scoresList).hasSize(databaseSizeBeforeUpdate);
        Scores testScores = scoresList.get(scoresList.size() - 1);
        assertThat(testScores.getScore()).isEqualTo(UPDATED_SCORE);
    }

    @Test
    @Transactional
    public void updateNonExistingScores() throws Exception {
        int databaseSizeBeforeUpdate = scoresRepository.findAll().size();

        // Create the Scores
        ScoresDTO scoresDTO = scoresMapper.toDto(scores);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restScoresMockMvc.perform(put("/api/scores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scoresDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Scores in the database
        List<Scores> scoresList = scoresRepository.findAll();
        assertThat(scoresList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteScores() throws Exception {
        // Initialize the database
        scoresRepository.saveAndFlush(scores);

        int databaseSizeBeforeDelete = scoresRepository.findAll().size();

        // Delete the scores
        restScoresMockMvc.perform(delete("/api/scores/{id}", scores.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Scores> scoresList = scoresRepository.findAll();
        assertThat(scoresList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Scores.class);
        Scores scores1 = new Scores();
        scores1.setId(1L);
        Scores scores2 = new Scores();
        scores2.setId(scores1.getId());
        assertThat(scores1).isEqualTo(scores2);
        scores2.setId(2L);
        assertThat(scores1).isNotEqualTo(scores2);
        scores1.setId(null);
        assertThat(scores1).isNotEqualTo(scores2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ScoresDTO.class);
        ScoresDTO scoresDTO1 = new ScoresDTO();
        scoresDTO1.setId(1L);
        ScoresDTO scoresDTO2 = new ScoresDTO();
        assertThat(scoresDTO1).isNotEqualTo(scoresDTO2);
        scoresDTO2.setId(scoresDTO1.getId());
        assertThat(scoresDTO1).isEqualTo(scoresDTO2);
        scoresDTO2.setId(2L);
        assertThat(scoresDTO1).isNotEqualTo(scoresDTO2);
        scoresDTO1.setId(null);
        assertThat(scoresDTO1).isNotEqualTo(scoresDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(scoresMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(scoresMapper.fromId(null)).isNull();
    }
}
