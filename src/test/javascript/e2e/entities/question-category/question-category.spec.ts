/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { QuestionCategoryComponentsPage, QuestionCategoryDeleteDialog, QuestionCategoryUpdatePage } from './question-category.page-object';

const expect = chai.expect;

describe('QuestionCategory e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let questionCategoryUpdatePage: QuestionCategoryUpdatePage;
    let questionCategoryComponentsPage: QuestionCategoryComponentsPage;
    let questionCategoryDeleteDialog: QuestionCategoryDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load QuestionCategories', async () => {
        await navBarPage.goToEntity('question-category');
        questionCategoryComponentsPage = new QuestionCategoryComponentsPage();
        await browser.wait(ec.visibilityOf(questionCategoryComponentsPage.title), 5000);
        expect(await questionCategoryComponentsPage.getTitle()).to.eq('Question Categories');
    });

    it('should load create QuestionCategory page', async () => {
        await questionCategoryComponentsPage.clickOnCreateButton();
        questionCategoryUpdatePage = new QuestionCategoryUpdatePage();
        expect(await questionCategoryUpdatePage.getPageTitle()).to.eq('Create or edit a Question Category');
        await questionCategoryUpdatePage.cancel();
    });

    it('should create and save QuestionCategories', async () => {
        const nbButtonsBeforeCreate = await questionCategoryComponentsPage.countDeleteButtons();

        await questionCategoryComponentsPage.clickOnCreateButton();
        await promise.all([questionCategoryUpdatePage.setNameInput('name')]);
        expect(await questionCategoryUpdatePage.getNameInput()).to.eq('name');
        await questionCategoryUpdatePage.save();
        expect(await questionCategoryUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await questionCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last QuestionCategory', async () => {
        const nbButtonsBeforeDelete = await questionCategoryComponentsPage.countDeleteButtons();
        await questionCategoryComponentsPage.clickOnLastDeleteButton();

        questionCategoryDeleteDialog = new QuestionCategoryDeleteDialog();
        expect(await questionCategoryDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Question Category?');
        await questionCategoryDeleteDialog.clickOnConfirmButton();

        expect(await questionCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
