import { element, by, ElementFinder } from 'protractor';

export class QuestionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-question div table .btn-danger'));
    title = element.all(by.css('jhi-question div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class QuestionUpdatePage {
    pageTitle = element(by.id('jhi-question-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    contentInput = element(by.id('field_content'));
    questioncategorySelect = element(by.id('field_questioncategory'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setContentInput(content) {
        await this.contentInput.sendKeys(content);
    }

    async getContentInput() {
        return this.contentInput.getAttribute('value');
    }

    async questioncategorySelectLastOption() {
        await this.questioncategorySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async questioncategorySelectOption(option) {
        await this.questioncategorySelect.sendKeys(option);
    }

    getQuestioncategorySelect(): ElementFinder {
        return this.questioncategorySelect;
    }

    async getQuestioncategorySelectedOption() {
        return this.questioncategorySelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class QuestionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-question-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-question'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
