/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CoursesComponentsPage, CoursesDeleteDialog, CoursesUpdatePage } from './courses.page-object';

const expect = chai.expect;

describe('Courses e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let coursesUpdatePage: CoursesUpdatePage;
    let coursesComponentsPage: CoursesComponentsPage;
    let coursesDeleteDialog: CoursesDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Courses', async () => {
        await navBarPage.goToEntity('courses');
        coursesComponentsPage = new CoursesComponentsPage();
        await browser.wait(ec.visibilityOf(coursesComponentsPage.title), 5000);
        expect(await coursesComponentsPage.getTitle()).to.eq('Courses');
    });

    it('should load create Courses page', async () => {
        await coursesComponentsPage.clickOnCreateButton();
        coursesUpdatePage = new CoursesUpdatePage();
        expect(await coursesUpdatePage.getPageTitle()).to.eq('Create or edit a Courses');
        await coursesUpdatePage.cancel();
    });

    it('should create and save Courses', async () => {
        const nbButtonsBeforeCreate = await coursesComponentsPage.countDeleteButtons();

        await coursesComponentsPage.clickOnCreateButton();
        await promise.all([
            coursesUpdatePage.setTitleInput('title'),
            coursesUpdatePage.setCodeInput('code'),
            coursesUpdatePage.departmentSelectLastOption()
        ]);
        expect(await coursesUpdatePage.getTitleInput()).to.eq('title');
        expect(await coursesUpdatePage.getCodeInput()).to.eq('code');
        await coursesUpdatePage.save();
        expect(await coursesUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await coursesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Courses', async () => {
        const nbButtonsBeforeDelete = await coursesComponentsPage.countDeleteButtons();
        await coursesComponentsPage.clickOnLastDeleteButton();

        coursesDeleteDialog = new CoursesDeleteDialog();
        expect(await coursesDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Courses?');
        await coursesDeleteDialog.clickOnConfirmButton();

        expect(await coursesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
