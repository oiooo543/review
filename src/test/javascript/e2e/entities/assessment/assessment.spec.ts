/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AssessmentComponentsPage, AssessmentDeleteDialog, AssessmentUpdatePage } from './assessment.page-object';

const expect = chai.expect;

describe('Assessment e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let assessmentUpdatePage: AssessmentUpdatePage;
    let assessmentComponentsPage: AssessmentComponentsPage;
    let assessmentDeleteDialog: AssessmentDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Assessments', async () => {
        await navBarPage.goToEntity('assessment');
        assessmentComponentsPage = new AssessmentComponentsPage();
        await browser.wait(ec.visibilityOf(assessmentComponentsPage.title), 5000);
        expect(await assessmentComponentsPage.getTitle()).to.eq('Assessments');
    });

    it('should load create Assessment page', async () => {
        await assessmentComponentsPage.clickOnCreateButton();
        assessmentUpdatePage = new AssessmentUpdatePage();
        expect(await assessmentUpdatePage.getPageTitle()).to.eq('Create or edit a Assessment');
        await assessmentUpdatePage.cancel();
    });

    it('should create and save Assessments', async () => {
        const nbButtonsBeforeCreate = await assessmentComponentsPage.countDeleteButtons();

        await assessmentComponentsPage.clickOnCreateButton();
        await promise.all([]);
        await assessmentUpdatePage.save();
        expect(await assessmentUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await assessmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Assessment', async () => {
        const nbButtonsBeforeDelete = await assessmentComponentsPage.countDeleteButtons();
        await assessmentComponentsPage.clickOnLastDeleteButton();

        assessmentDeleteDialog = new AssessmentDeleteDialog();
        expect(await assessmentDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Assessment?');
        await assessmentDeleteDialog.clickOnConfirmButton();

        expect(await assessmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
