import { element, by, ElementFinder } from 'protractor';

export class LecturerComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-lecturer div table .btn-danger'));
    title = element.all(by.css('jhi-lecturer div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class LecturerUpdatePage {
    pageTitle = element(by.id('jhi-lecturer-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nameInput = element(by.id('field_name'));
    coursesSelect = element(by.id('field_courses'));
    departmentSelect = element(by.id('field_department'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async coursesSelectLastOption() {
        await this.coursesSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async coursesSelectOption(option) {
        await this.coursesSelect.sendKeys(option);
    }

    getCoursesSelect(): ElementFinder {
        return this.coursesSelect;
    }

    async getCoursesSelectedOption() {
        return this.coursesSelect.element(by.css('option:checked')).getText();
    }

    async departmentSelectLastOption() {
        await this.departmentSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async departmentSelectOption(option) {
        await this.departmentSelect.sendKeys(option);
    }

    getDepartmentSelect(): ElementFinder {
        return this.departmentSelect;
    }

    async getDepartmentSelectedOption() {
        return this.departmentSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class LecturerDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-lecturer-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-lecturer'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
