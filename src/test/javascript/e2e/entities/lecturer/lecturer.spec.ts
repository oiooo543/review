/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LecturerComponentsPage, LecturerDeleteDialog, LecturerUpdatePage } from './lecturer.page-object';

const expect = chai.expect;

describe('Lecturer e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let lecturerUpdatePage: LecturerUpdatePage;
    let lecturerComponentsPage: LecturerComponentsPage;
    let lecturerDeleteDialog: LecturerDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Lecturers', async () => {
        await navBarPage.goToEntity('lecturer');
        lecturerComponentsPage = new LecturerComponentsPage();
        await browser.wait(ec.visibilityOf(lecturerComponentsPage.title), 5000);
        expect(await lecturerComponentsPage.getTitle()).to.eq('Lecturers');
    });

    it('should load create Lecturer page', async () => {
        await lecturerComponentsPage.clickOnCreateButton();
        lecturerUpdatePage = new LecturerUpdatePage();
        expect(await lecturerUpdatePage.getPageTitle()).to.eq('Create or edit a Lecturer');
        await lecturerUpdatePage.cancel();
    });

    it('should create and save Lecturers', async () => {
        const nbButtonsBeforeCreate = await lecturerComponentsPage.countDeleteButtons();

        await lecturerComponentsPage.clickOnCreateButton();
        await promise.all([
            lecturerUpdatePage.setNameInput('name'),
            // lecturerUpdatePage.coursesSelectLastOption(),
            lecturerUpdatePage.departmentSelectLastOption()
        ]);
        expect(await lecturerUpdatePage.getNameInput()).to.eq('name');
        await lecturerUpdatePage.save();
        expect(await lecturerUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await lecturerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Lecturer', async () => {
        const nbButtonsBeforeDelete = await lecturerComponentsPage.countDeleteButtons();
        await lecturerComponentsPage.clickOnLastDeleteButton();

        lecturerDeleteDialog = new LecturerDeleteDialog();
        expect(await lecturerDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Lecturer?');
        await lecturerDeleteDialog.clickOnConfirmButton();

        expect(await lecturerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
