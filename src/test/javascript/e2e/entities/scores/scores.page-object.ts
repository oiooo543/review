import { element, by, ElementFinder } from 'protractor';

export class ScoresComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-scores div table .btn-danger'));
    title = element.all(by.css('jhi-scores div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ScoresUpdatePage {
    pageTitle = element(by.id('jhi-scores-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    scoreInput = element(by.id('field_score'));
    lecturerSelect = element(by.id('field_lecturer'));
    courseSelect = element(by.id('field_course'));
    questioncategorySelect = element(by.id('field_questioncategory'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setScoreInput(score) {
        await this.scoreInput.sendKeys(score);
    }

    async getScoreInput() {
        return this.scoreInput.getAttribute('value');
    }

    async lecturerSelectLastOption() {
        await this.lecturerSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async lecturerSelectOption(option) {
        await this.lecturerSelect.sendKeys(option);
    }

    getLecturerSelect(): ElementFinder {
        return this.lecturerSelect;
    }

    async getLecturerSelectedOption() {
        return this.lecturerSelect.element(by.css('option:checked')).getText();
    }

    async courseSelectLastOption() {
        await this.courseSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async courseSelectOption(option) {
        await this.courseSelect.sendKeys(option);
    }

    getCourseSelect(): ElementFinder {
        return this.courseSelect;
    }

    async getCourseSelectedOption() {
        return this.courseSelect.element(by.css('option:checked')).getText();
    }

    async questioncategorySelectLastOption() {
        await this.questioncategorySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async questioncategorySelectOption(option) {
        await this.questioncategorySelect.sendKeys(option);
    }

    getQuestioncategorySelect(): ElementFinder {
        return this.questioncategorySelect;
    }

    async getQuestioncategorySelectedOption() {
        return this.questioncategorySelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ScoresDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-scores-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-scores'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
