import { element, by, ElementFinder } from 'protractor';

export class DepartmentComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-department div table .btn-danger'));
    title = element.all(by.css('jhi-department div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class DepartmentUpdatePage {
    pageTitle = element(by.id('jhi-department-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nameInput = element(by.id('field_name'));
    facultySelect = element(by.id('field_faculty'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNameInput(name) {
        await this.nameInput.sendKeys(name);
    }

    async getNameInput() {
        return this.nameInput.getAttribute('value');
    }

    async facultySelectLastOption() {
        await this.facultySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async facultySelectOption(option) {
        await this.facultySelect.sendKeys(option);
    }

    getFacultySelect(): ElementFinder {
        return this.facultySelect;
    }

    async getFacultySelectedOption() {
        return this.facultySelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class DepartmentDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-department-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-department'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
