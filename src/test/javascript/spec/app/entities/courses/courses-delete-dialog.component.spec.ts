/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HmTestModule } from '../../../test.module';
import { CoursesDeleteDialogComponent } from 'app/entities/courses/courses-delete-dialog.component';
import { CoursesService } from 'app/entities/courses/courses.service';

describe('Component Tests', () => {
    describe('Courses Management Delete Component', () => {
        let comp: CoursesDeleteDialogComponent;
        let fixture: ComponentFixture<CoursesDeleteDialogComponent>;
        let service: CoursesService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [CoursesDeleteDialogComponent]
            })
                .overrideTemplate(CoursesDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CoursesDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CoursesService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
