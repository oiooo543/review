/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HmTestModule } from '../../../test.module';
import { CoursesDetailComponent } from 'app/entities/courses/courses-detail.component';
import { Courses } from 'app/shared/model/courses.model';

describe('Component Tests', () => {
    describe('Courses Management Detail Component', () => {
        let comp: CoursesDetailComponent;
        let fixture: ComponentFixture<CoursesDetailComponent>;
        const route = ({ data: of({ courses: new Courses(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [CoursesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CoursesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CoursesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.courses).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
