/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HmTestModule } from '../../../test.module';
import { QuestionCategoryComponent } from 'app/entities/question-category/question-category.component';
import { QuestionCategoryService } from 'app/entities/question-category/question-category.service';
import { QuestionCategory } from 'app/shared/model/question-category.model';

describe('Component Tests', () => {
    describe('QuestionCategory Management Component', () => {
        let comp: QuestionCategoryComponent;
        let fixture: ComponentFixture<QuestionCategoryComponent>;
        let service: QuestionCategoryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [QuestionCategoryComponent],
                providers: []
            })
                .overrideTemplate(QuestionCategoryComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(QuestionCategoryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuestionCategoryService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new QuestionCategory(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.questionCategories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
