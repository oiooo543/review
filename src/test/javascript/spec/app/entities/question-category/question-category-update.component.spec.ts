/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { HmTestModule } from '../../../test.module';
import { QuestionCategoryUpdateComponent } from 'app/entities/question-category/question-category-update.component';
import { QuestionCategoryService } from 'app/entities/question-category/question-category.service';
import { QuestionCategory } from 'app/shared/model/question-category.model';

describe('Component Tests', () => {
    describe('QuestionCategory Management Update Component', () => {
        let comp: QuestionCategoryUpdateComponent;
        let fixture: ComponentFixture<QuestionCategoryUpdateComponent>;
        let service: QuestionCategoryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [QuestionCategoryUpdateComponent]
            })
                .overrideTemplate(QuestionCategoryUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(QuestionCategoryUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuestionCategoryService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new QuestionCategory(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.questionCategory = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new QuestionCategory();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.questionCategory = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
