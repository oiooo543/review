/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HmTestModule } from '../../../test.module';
import { QuestionCategoryDetailComponent } from 'app/entities/question-category/question-category-detail.component';
import { QuestionCategory } from 'app/shared/model/question-category.model';

describe('Component Tests', () => {
    describe('QuestionCategory Management Detail Component', () => {
        let comp: QuestionCategoryDetailComponent;
        let fixture: ComponentFixture<QuestionCategoryDetailComponent>;
        const route = ({ data: of({ questionCategory: new QuestionCategory(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [QuestionCategoryDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(QuestionCategoryDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuestionCategoryDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.questionCategory).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
