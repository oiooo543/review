/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HmTestModule } from '../../../test.module';
import { QuestionCategoryDeleteDialogComponent } from 'app/entities/question-category/question-category-delete-dialog.component';
import { QuestionCategoryService } from 'app/entities/question-category/question-category.service';

describe('Component Tests', () => {
    describe('QuestionCategory Management Delete Component', () => {
        let comp: QuestionCategoryDeleteDialogComponent;
        let fixture: ComponentFixture<QuestionCategoryDeleteDialogComponent>;
        let service: QuestionCategoryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [QuestionCategoryDeleteDialogComponent]
            })
                .overrideTemplate(QuestionCategoryDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(QuestionCategoryDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(QuestionCategoryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
