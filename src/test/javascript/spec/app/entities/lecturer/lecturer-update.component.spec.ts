/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { HmTestModule } from '../../../test.module';
import { LecturerUpdateComponent } from 'app/entities/lecturer/lecturer-update.component';
import { LecturerService } from 'app/entities/lecturer/lecturer.service';
import { Lecturer } from 'app/shared/model/lecturer.model';

describe('Component Tests', () => {
    describe('Lecturer Management Update Component', () => {
        let comp: LecturerUpdateComponent;
        let fixture: ComponentFixture<LecturerUpdateComponent>;
        let service: LecturerService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [LecturerUpdateComponent]
            })
                .overrideTemplate(LecturerUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LecturerUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LecturerService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Lecturer(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.lecturer = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Lecturer();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.lecturer = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
