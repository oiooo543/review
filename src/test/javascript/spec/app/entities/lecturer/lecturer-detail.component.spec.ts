/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HmTestModule } from '../../../test.module';
import { LecturerDetailComponent } from 'app/entities/lecturer/lecturer-detail.component';
import { Lecturer } from 'app/shared/model/lecturer.model';

describe('Component Tests', () => {
    describe('Lecturer Management Detail Component', () => {
        let comp: LecturerDetailComponent;
        let fixture: ComponentFixture<LecturerDetailComponent>;
        const route = ({ data: of({ lecturer: new Lecturer(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [LecturerDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LecturerDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LecturerDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.lecturer).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
