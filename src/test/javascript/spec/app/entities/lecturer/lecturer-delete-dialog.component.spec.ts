/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HmTestModule } from '../../../test.module';
import { LecturerDeleteDialogComponent } from 'app/entities/lecturer/lecturer-delete-dialog.component';
import { LecturerService } from 'app/entities/lecturer/lecturer.service';

describe('Component Tests', () => {
    describe('Lecturer Management Delete Component', () => {
        let comp: LecturerDeleteDialogComponent;
        let fixture: ComponentFixture<LecturerDeleteDialogComponent>;
        let service: LecturerService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HmTestModule],
                declarations: [LecturerDeleteDialogComponent]
            })
                .overrideTemplate(LecturerDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LecturerDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LecturerService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
