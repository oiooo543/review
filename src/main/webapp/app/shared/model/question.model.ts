export interface IQuestion {
    id?: number;
    content?: string;
    questioncategoryId?: number;
}

export class Question implements IQuestion {
    constructor(public id?: number, public content?: string, public questioncategoryId?: number) {}
}
