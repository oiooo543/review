import { IStudent } from 'app/shared/model/student.model';
import { ICourses } from 'app/shared/model/courses.model';
import { ILecturer } from 'app/shared/model/lecturer.model';
import { IProgram } from 'app/shared/model/program.model';

export interface IDepartment {
    id?: number;
    name?: string;
    students?: IStudent[];
    courses?: ICourses[];
    lecturers?: ILecturer[];
    programs?: IProgram[];
    facultyId?: number;
}

export class Department implements IDepartment {
    constructor(
        public id?: number,
        public name?: string,
        public students?: IStudent[],
        public courses?: ICourses[],
        public lecturers?: ILecturer[],
        public programs?: IProgram[],
        public facultyId?: number
    ) {}
}
