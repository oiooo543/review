export interface IProgram {
    id?: number;
    name?: string;
    departmentId?: number;
}

export class Program implements IProgram {
    constructor(public id?: number, public name?: string, public departmentId?: number) {}
}
