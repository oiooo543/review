import { IScores } from 'app/shared/model/scores.model';
import { ILecturer } from 'app/shared/model/lecturer.model';

export interface ICourses {
    id?: number;
    title?: string;
    code?: string;
    scores?: IScores[];
    departmentId?: number;
    lecturers?: ILecturer[];
}

export class Courses implements ICourses {
    constructor(
        public id?: number,
        public title?: string,
        public code?: string,
        public scores?: IScores[],
        public departmentId?: number,
        public lecturers?: ILecturer[]
    ) {}
}
