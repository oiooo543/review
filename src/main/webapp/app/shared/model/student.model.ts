export interface IStudent {
    id?: number;
    level?: string;
    departmentId?: number;
}

export class Student implements IStudent {
    constructor(public id?: number, public level?: string, public departmentId?: number) {}
}
