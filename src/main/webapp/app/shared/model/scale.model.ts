

export interface ICase {
    id?: number,
    name?: string
}

export class Case implements ICase {
    constructor(
       public id?: number,
      public name?: string
    ) {}
}
