import { IQuestion } from 'app/shared/model/question.model';
import { IScores } from 'app/shared/model/scores.model';

export interface IQuestionCategory {
    id?: number;
    name?: string;
    questions?: IQuestion[];
    scores?: IScores[];
}

export class QuestionCategory implements IQuestionCategory {
    constructor(public id?: number, public name?: string, public questions?: IQuestion[], public scores?: IScores[]) {}
}
