export interface IScores {
    id?: number;
    score?: number;
    lecturerId?: number;
    courseId?: number;
    questioncategoryId?: number;
}

export class Scores implements IScores {
    constructor(
        public id?: number,
        public score?: number,
        public lecturerId?: number,
        public courseId?: number,
        public questioncategoryId?: number
    ) {}
}
