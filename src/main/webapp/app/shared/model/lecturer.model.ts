import { IScores } from 'app/shared/model/scores.model';
import { ICourses } from 'app/shared/model/courses.model';

export interface ILecturer {
    id?: number;
    name?: string;
    scores?: IScores[];
    courses?: ICourses[];
    departmentId?: number;
}

export class Lecturer implements ILecturer {
    constructor(
        public id?: number,
        public name?: string,
        public scores?: IScores[],
        public courses?: ICourses[],
        public departmentId?: number
    ) {}
}
