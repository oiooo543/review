export interface IAssessment {
    id?: number;
    courseId?: number;
    lecturerId?: number;
    questioncategoryId?: number;
    score?: number;
    sub?: number;
    rel?: number;
    example?: number;
    add?: number;
    prep?: number;
    approp?: number;
    enco?: number;
    ident?: number;
    express?: number;
    resp?: number;
    deal?: number;
    stim?: number;
    acc?: number;
    open?: number;
    treat?: number;
    lect?: number;
    regular?: number;
    kept?: number;
    scrip?: number;
    overall?: number;
    seats?: number;
    illum?: number;
    vent?: number;
    pas?: number;
}

export class Assessment implements IAssessment {
    constructor(
        public id?: number,
        public sub?: number,
        public rel?: number,
        public courseId?: number,
        public lecturerId?: number,
        public questioncategoryId?: number,
        public score?: number,
        public example?: number,
        public add?: number,
        public prep?: number,
        public approp?: number,
        public enco?: number,
        public ident?: number,
        public express?: number,
        public resp?: number,
        public deal?: number,
        public stim?: number,
        public acc?: number,
        public open?: number,
        public treat?: number,
        public lect?: number,
        public regular?: number,
        public kept?: number,
        public scrip?: number,
        public overall?: number,
        public seats?: number,
        public illum?: number,
        public vent?: number,
        public pas?: number

    ) {}
}
