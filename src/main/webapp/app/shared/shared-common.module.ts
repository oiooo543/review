import { NgModule } from '@angular/core';

import { HmSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [HmSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [HmSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class HmSharedCommonModule {}
