import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICourses } from 'app/shared/model/courses.model';
import { CoursesService } from './courses.service';

@Component({
    selector: 'jhi-courses-delete-dialog',
    templateUrl: './courses-delete-dialog.component.html'
})
export class CoursesDeleteDialogComponent {
    courses: ICourses;

    constructor(protected coursesService: CoursesService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.coursesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'coursesListModification',
                content: 'Deleted an courses'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-courses-delete-popup',
    template: ''
})
export class CoursesDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ courses }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CoursesDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.courses = courses;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/courses', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/courses', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
