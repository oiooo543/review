import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HmSharedModule } from 'app/shared';
import {
    CoursesComponent,
    CoursesDetailComponent,
    CoursesUpdateComponent,
    CoursesDeletePopupComponent,
    CoursesDeleteDialogComponent,
    coursesRoute,
    coursesPopupRoute
} from './';

const ENTITY_STATES = [...coursesRoute, ...coursesPopupRoute];

@NgModule({
    imports: [HmSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CoursesComponent,
        CoursesDetailComponent,
        CoursesUpdateComponent,
        CoursesDeleteDialogComponent,
        CoursesDeletePopupComponent
    ],
    entryComponents: [CoursesComponent, CoursesUpdateComponent, CoursesDeleteDialogComponent, CoursesDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HmCoursesModule {}
