import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICourses } from 'app/shared/model/courses.model';
import { CoursesService } from './courses.service';
import { IDepartment } from 'app/shared/model/department.model';
import { DepartmentService } from 'app/entities/department';
import { ILecturer } from 'app/shared/model/lecturer.model';
import { LecturerService } from 'app/entities/lecturer';

@Component({
    selector: 'jhi-courses-update',
    templateUrl: './courses-update.component.html'
})
export class CoursesUpdateComponent implements OnInit {
    courses: ICourses;
    isSaving: boolean;

    departments: IDepartment[];

    lecturers: ILecturer[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected coursesService: CoursesService,
        protected departmentService: DepartmentService,
        protected lecturerService: LecturerService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ courses }) => {
            this.courses = courses;
        });
        this.departmentService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IDepartment[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDepartment[]>) => response.body)
            )
            .subscribe((res: IDepartment[]) => (this.departments = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.lecturerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ILecturer[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILecturer[]>) => response.body)
            )
            .subscribe((res: ILecturer[]) => (this.lecturers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.courses.id !== undefined) {
            this.subscribeToSaveResponse(this.coursesService.update(this.courses));
        } else {
            this.subscribeToSaveResponse(this.coursesService.create(this.courses));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICourses>>) {
        result.subscribe((res: HttpResponse<ICourses>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDepartmentById(index: number, item: IDepartment) {
        return item.id;
    }

    trackLecturerById(index: number, item: ILecturer) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
