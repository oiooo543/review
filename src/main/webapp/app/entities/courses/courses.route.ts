import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Courses } from 'app/shared/model/courses.model';
import { CoursesService } from './courses.service';
import { CoursesComponent } from './courses.component';
import { CoursesDetailComponent } from './courses-detail.component';
import { CoursesUpdateComponent } from './courses-update.component';
import { CoursesDeletePopupComponent } from './courses-delete-dialog.component';
import { ICourses } from 'app/shared/model/courses.model';

@Injectable({ providedIn: 'root' })
export class CoursesResolve implements Resolve<ICourses> {
    constructor(private service: CoursesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICourses> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Courses>) => response.ok),
                map((courses: HttpResponse<Courses>) => courses.body)
            );
        }
        return of(new Courses());
    }
}

export const coursesRoute: Routes = [
    {
        path: '',
        component: CoursesComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'Courses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CoursesDetailComponent,
        resolve: {
            courses: CoursesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Courses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CoursesUpdateComponent,
        resolve: {
            courses: CoursesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Courses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CoursesUpdateComponent,
        resolve: {
            courses: CoursesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Courses'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const coursesPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CoursesDeletePopupComponent,
        resolve: {
            courses: CoursesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Courses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
