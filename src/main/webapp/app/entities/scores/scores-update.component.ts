import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IScores } from 'app/shared/model/scores.model';
import { ScoresService } from './scores.service';
import { ILecturer } from 'app/shared/model/lecturer.model';
import { LecturerService } from 'app/entities/lecturer';
import { ICourses } from 'app/shared/model/courses.model';
import { CoursesService } from 'app/entities/courses';
import { IQuestionCategory } from 'app/shared/model/question-category.model';
import { QuestionCategoryService } from 'app/entities/question-category';

@Component({
    selector: 'jhi-scores-update',
    templateUrl: './scores-update.component.html'
})
export class ScoresUpdateComponent implements OnInit {
    scores: IScores;
    isSaving: boolean;

    lecturers: ILecturer[];

    courses: ICourses[];

    questioncategories: IQuestionCategory[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected scoresService: ScoresService,
        protected lecturerService: LecturerService,
        protected coursesService: CoursesService,
        protected questionCategoryService: QuestionCategoryService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ scores }) => {
            this.scores = scores;
        });
        this.lecturerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ILecturer[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILecturer[]>) => response.body)
            )
            .subscribe((res: ILecturer[]) => (this.lecturers = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.coursesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICourses[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICourses[]>) => response.body)
            )
            .subscribe((res: ICourses[]) => (this.courses = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.questionCategoryService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestionCategory[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestionCategory[]>) => response.body)
            )
            .subscribe(
                (res: IQuestionCategory[]) => (this.questioncategories = res),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.scores.id !== undefined) {
            this.subscribeToSaveResponse(this.scoresService.update(this.scores));
        } else {
            this.subscribeToSaveResponse(this.scoresService.create(this.scores));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IScores>>) {
        result.subscribe((res: HttpResponse<IScores>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackLecturerById(index: number, item: ILecturer) {
        return item.id;
    }

    trackCoursesById(index: number, item: ICourses) {
        return item.id;
    }

    trackQuestionCategoryById(index: number, item: IQuestionCategory) {
        return item.id;
    }
}
