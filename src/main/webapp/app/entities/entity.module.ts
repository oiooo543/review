import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'student',
                loadChildren: './student/student.module#HmStudentModule'
            },
            {
                path: 'department',
                loadChildren: './department/department.module#HmDepartmentModule'
            },
            {
                path: 'faculty',
                loadChildren: './faculty/faculty.module#HmFacultyModule'
            },
            {
                path: 'courses',
                loadChildren: './courses/courses.module#HmCoursesModule'
            },
            {
                path: 'lecturer',
                loadChildren: './lecturer/lecturer.module#HmLecturerModule'
            },
            {
                path: 'question-category',
                loadChildren: './question-category/question-category.module#HmQuestionCategoryModule'
            },
            {
                path: 'question',
                loadChildren: './question/question.module#HmQuestionModule'
            },
            {
                path: 'scores',
                loadChildren: './scores/scores.module#HmScoresModule'
            },
            {
                path: 'program',
                loadChildren: './program/program.module#HmProgramModule'
            },
            {
                path: 'assessment',
                loadChildren: './assessment/assessment.module#HmAssessmentModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HmEntityModule {}
