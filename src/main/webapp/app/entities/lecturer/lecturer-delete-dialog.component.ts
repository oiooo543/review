import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILecturer } from 'app/shared/model/lecturer.model';
import { LecturerService } from './lecturer.service';

@Component({
    selector: 'jhi-lecturer-delete-dialog',
    templateUrl: './lecturer-delete-dialog.component.html'
})
export class LecturerDeleteDialogComponent {
    lecturer: ILecturer;

    constructor(protected lecturerService: LecturerService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lecturerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'lecturerListModification',
                content: 'Deleted an lecturer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lecturer-delete-popup',
    template: ''
})
export class LecturerDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ lecturer }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(LecturerDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.lecturer = lecturer;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/lecturer', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/lecturer', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
