import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HmSharedModule } from 'app/shared';
import {
    LecturerComponent,
    LecturerDetailComponent,
    LecturerUpdateComponent,
    LecturerDeletePopupComponent,
    LecturerDeleteDialogComponent,
    lecturerRoute,
    lecturerPopupRoute
} from './';

const ENTITY_STATES = [...lecturerRoute, ...lecturerPopupRoute];

@NgModule({
    imports: [HmSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LecturerComponent,
        LecturerDetailComponent,
        LecturerUpdateComponent,
        LecturerDeleteDialogComponent,
        LecturerDeletePopupComponent
    ],
    entryComponents: [LecturerComponent, LecturerUpdateComponent, LecturerDeleteDialogComponent, LecturerDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HmLecturerModule {}
