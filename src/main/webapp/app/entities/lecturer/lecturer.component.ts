import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ILecturer } from 'app/shared/model/lecturer.model';
import { AccountService } from 'app/core';
import { LecturerService } from './lecturer.service';

@Component({
    selector: 'jhi-lecturer',
    templateUrl: './lecturer.component.html'
})
export class LecturerComponent implements OnInit, OnDestroy {
    lecturers: ILecturer[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected lecturerService: LecturerService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.lecturerService
            .query()
            .pipe(
                filter((res: HttpResponse<ILecturer[]>) => res.ok),
                map((res: HttpResponse<ILecturer[]>) => res.body)
            )
            .subscribe(
                (res: ILecturer[]) => {
                    this.lecturers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInLecturers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ILecturer) {
        return item.id;
    }

    registerChangeInLecturers() {
        this.eventSubscriber = this.eventManager.subscribe('lecturerListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
