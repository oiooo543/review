import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Lecturer } from 'app/shared/model/lecturer.model';
import { LecturerService } from './lecturer.service';
import { LecturerComponent } from './lecturer.component';
import { LecturerDetailComponent } from './lecturer-detail.component';
import { LecturerUpdateComponent } from './lecturer-update.component';
import { LecturerDeletePopupComponent } from './lecturer-delete-dialog.component';
import { ILecturer } from 'app/shared/model/lecturer.model';

@Injectable({ providedIn: 'root' })
export class LecturerResolve implements Resolve<ILecturer> {
    constructor(private service: LecturerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILecturer> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Lecturer>) => response.ok),
                map((lecturer: HttpResponse<Lecturer>) => lecturer.body)
            );
        }
        return of(new Lecturer());
    }
}

export const lecturerRoute: Routes = [
    {
        path: '',
        component: LecturerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lecturers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: LecturerDetailComponent,
        resolve: {
            lecturer: LecturerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lecturers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: LecturerUpdateComponent,
        resolve: {
            lecturer: LecturerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lecturers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: LecturerUpdateComponent,
        resolve: {
            lecturer: LecturerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lecturers'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lecturerPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: LecturerDeletePopupComponent,
        resolve: {
            lecturer: LecturerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lecturers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
