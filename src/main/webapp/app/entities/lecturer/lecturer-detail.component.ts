import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILecturer } from 'app/shared/model/lecturer.model';

@Component({
    selector: 'jhi-lecturer-detail',
    templateUrl: './lecturer-detail.component.html'
})
export class LecturerDetailComponent implements OnInit {
    lecturer: ILecturer;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ lecturer }) => {
            this.lecturer = lecturer;
        });
    }

    previousState() {
        window.history.back();
    }
}
