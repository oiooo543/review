import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ILecturer } from 'app/shared/model/lecturer.model';
import { LecturerService } from './lecturer.service';
import { ICourses } from 'app/shared/model/courses.model';
import { CoursesService } from 'app/entities/courses';
import { IDepartment } from 'app/shared/model/department.model';
import { DepartmentService } from 'app/entities/department';

@Component({
    selector: 'jhi-lecturer-update',
    templateUrl: './lecturer-update.component.html'
})
export class LecturerUpdateComponent implements OnInit {
     lecturer: ILecturer;
    isSaving: boolean;

    courses: ICourses[];

    departments: IDepartment[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected lecturerService: LecturerService,
        protected coursesService: CoursesService,
        protected departmentService: DepartmentService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ lecturer }) => {
            this.lecturer = lecturer;
        });
        this.coursesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICourses[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICourses[]>) => response.body)
            )
            .subscribe((res: ICourses[]) => (this.courses = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.departmentService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IDepartment[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDepartment[]>) => response.body)
            )
            .subscribe((res: IDepartment[]) => (this.departments = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.lecturer.id !== undefined) {
            this.subscribeToSaveResponse(this.lecturerService.update(this.lecturer));
        } else {
            this.subscribeToSaveResponse(this.lecturerService.create(this.lecturer));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILecturer>>) {
        result.subscribe((res: HttpResponse<ILecturer>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCoursesById(index: number, item: ICourses) {
        return item.id;
    }

    trackDepartmentById(index: number, item: IDepartment) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
