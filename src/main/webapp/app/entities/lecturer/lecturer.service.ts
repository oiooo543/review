import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import {SERVER_API_URL, SERVER_API_URLONE} from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILecturer } from 'app/shared/model/lecturer.model';

type EntityResponseType = HttpResponse<ILecturer>;
type EntityArrayResponseType = HttpResponse<ILecturer[]>;

@Injectable({ providedIn: 'root' })
export class LecturerService {
    public resourceUrl = SERVER_API_URL + 'api/lecturers';
    public resourseUrlOne = SERVER_API_URLONE + '/courselecturer/1';

    constructor(protected http: HttpClient) {}

    create(lecturer: ILecturer): Observable<EntityResponseType> {
        return this.http.post<ILecturer>(this.resourceUrl, lecturer, { observe: 'response' });
    }

    update(lecturer: ILecturer): Observable<EntityResponseType> {
        return this.http.put<ILecturer>(this.resourceUrl, lecturer, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ILecturer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ILecturer[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    take(id: number): Observable<EntityArrayResponseType> {
        return this.http.get<ILecturer[]>(this.resourseUrlOne, {observe: 'response'});
    }
}
