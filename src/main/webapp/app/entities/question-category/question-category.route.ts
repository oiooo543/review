import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { QuestionCategory } from 'app/shared/model/question-category.model';
import { QuestionCategoryService } from './question-category.service';
import { QuestionCategoryComponent } from './question-category.component';
import { QuestionCategoryDetailComponent } from './question-category-detail.component';
import { QuestionCategoryUpdateComponent } from './question-category-update.component';
import { QuestionCategoryDeletePopupComponent } from './question-category-delete-dialog.component';
import { IQuestionCategory } from 'app/shared/model/question-category.model';

@Injectable({ providedIn: 'root' })
export class QuestionCategoryResolve implements Resolve<IQuestionCategory> {
    constructor(private service: QuestionCategoryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IQuestionCategory> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<QuestionCategory>) => response.ok),
                map((questionCategory: HttpResponse<QuestionCategory>) => questionCategory.body)
            );
        }
        return of(new QuestionCategory());
    }
}

export const questionCategoryRoute: Routes = [
    {
        path: '',
        component: QuestionCategoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'QuestionCategories'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: QuestionCategoryDetailComponent,
        resolve: {
            questionCategory: QuestionCategoryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'QuestionCategories'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: QuestionCategoryUpdateComponent,
        resolve: {
            questionCategory: QuestionCategoryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'QuestionCategories'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: QuestionCategoryUpdateComponent,
        resolve: {
            questionCategory: QuestionCategoryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'QuestionCategories'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const questionCategoryPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: QuestionCategoryDeletePopupComponent,
        resolve: {
            questionCategory: QuestionCategoryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'QuestionCategories'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
