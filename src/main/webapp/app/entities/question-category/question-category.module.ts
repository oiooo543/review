import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HmSharedModule } from 'app/shared';
import {
    QuestionCategoryComponent,
    QuestionCategoryDetailComponent,
    QuestionCategoryUpdateComponent,
    QuestionCategoryDeletePopupComponent,
    QuestionCategoryDeleteDialogComponent,
    questionCategoryRoute,
    questionCategoryPopupRoute
} from './';

const ENTITY_STATES = [...questionCategoryRoute, ...questionCategoryPopupRoute];

@NgModule({
    imports: [HmSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        QuestionCategoryComponent,
        QuestionCategoryDetailComponent,
        QuestionCategoryUpdateComponent,
        QuestionCategoryDeleteDialogComponent,
        QuestionCategoryDeletePopupComponent
    ],
    entryComponents: [
        QuestionCategoryComponent,
        QuestionCategoryUpdateComponent,
        QuestionCategoryDeleteDialogComponent,
        QuestionCategoryDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HmQuestionCategoryModule {}
