export * from './question-category.service';
export * from './question-category-update.component';
export * from './question-category-delete-dialog.component';
export * from './question-category-detail.component';
export * from './question-category.component';
export * from './question-category.route';
