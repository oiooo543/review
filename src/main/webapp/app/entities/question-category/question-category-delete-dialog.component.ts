import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IQuestionCategory } from 'app/shared/model/question-category.model';
import { QuestionCategoryService } from './question-category.service';

@Component({
    selector: 'jhi-question-category-delete-dialog',
    templateUrl: './question-category-delete-dialog.component.html'
})
export class QuestionCategoryDeleteDialogComponent {
    questionCategory: IQuestionCategory;

    constructor(
        protected questionCategoryService: QuestionCategoryService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.questionCategoryService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'questionCategoryListModification',
                content: 'Deleted an questionCategory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-question-category-delete-popup',
    template: ''
})
export class QuestionCategoryDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ questionCategory }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(QuestionCategoryDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.questionCategory = questionCategory;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/question-category', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/question-category', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
