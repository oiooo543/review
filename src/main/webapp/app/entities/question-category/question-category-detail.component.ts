import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuestionCategory } from 'app/shared/model/question-category.model';

@Component({
    selector: 'jhi-question-category-detail',
    templateUrl: './question-category-detail.component.html'
})
export class QuestionCategoryDetailComponent implements OnInit {
    questionCategory: IQuestionCategory;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ questionCategory }) => {
            this.questionCategory = questionCategory;
        });
    }

    previousState() {
        window.history.back();
    }
}
