import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IQuestionCategory } from 'app/shared/model/question-category.model';
import { QuestionCategoryService } from './question-category.service';

@Component({
    selector: 'jhi-question-category-update',
    templateUrl: './question-category-update.component.html'
})
export class QuestionCategoryUpdateComponent implements OnInit {
    questionCategory: IQuestionCategory;
    isSaving: boolean;

    constructor(protected questionCategoryService: QuestionCategoryService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ questionCategory }) => {
            this.questionCategory = questionCategory;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.questionCategory.id !== undefined) {
            this.subscribeToSaveResponse(this.questionCategoryService.update(this.questionCategory));
        } else {
            this.subscribeToSaveResponse(this.questionCategoryService.create(this.questionCategory));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuestionCategory>>) {
        result.subscribe((res: HttpResponse<IQuestionCategory>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
