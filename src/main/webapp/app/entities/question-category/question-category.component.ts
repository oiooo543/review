import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IQuestionCategory } from 'app/shared/model/question-category.model';
import { AccountService } from 'app/core';
import { QuestionCategoryService } from './question-category.service';

@Component({
    selector: 'jhi-question-category',
    templateUrl: './question-category.component.html'
})
export class QuestionCategoryComponent implements OnInit, OnDestroy {
    questionCategories: IQuestionCategory[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected questionCategoryService: QuestionCategoryService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.questionCategoryService
            .query()
            .pipe(
                filter((res: HttpResponse<IQuestionCategory[]>) => res.ok),
                map((res: HttpResponse<IQuestionCategory[]>) => res.body)
            )
            .subscribe(
                (res: IQuestionCategory[]) => {
                    this.questionCategories = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInQuestionCategories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IQuestionCategory) {
        return item.id;
    }

    registerChangeInQuestionCategories() {
        this.eventSubscriber = this.eventManager.subscribe('questionCategoryListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
