import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IQuestionCategory } from 'app/shared/model/question-category.model';

type EntityResponseType = HttpResponse<IQuestionCategory>;
type EntityArrayResponseType = HttpResponse<IQuestionCategory[]>;

@Injectable({ providedIn: 'root' })
export class QuestionCategoryService {
    public resourceUrl = SERVER_API_URL + 'api/question-categories';

    constructor(protected http: HttpClient) {}

    create(questionCategory: IQuestionCategory): Observable<EntityResponseType> {
        return this.http.post<IQuestionCategory>(this.resourceUrl, questionCategory, { observe: 'response' });
    }

    update(questionCategory: IQuestionCategory): Observable<EntityResponseType> {
        return this.http.put<IQuestionCategory>(this.resourceUrl, questionCategory, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IQuestionCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IQuestionCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
