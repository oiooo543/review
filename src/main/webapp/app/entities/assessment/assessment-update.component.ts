import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IAssessment } from 'app/shared/model/assessment.model';
import { AssessmentService } from './assessment.service';
import {IFaculty} from 'app/shared/model/faculty.model';
import {FacultyService} from 'app/entities/faculty';
import {JhiAlertService} from 'ng-jhipster';
import {IDepartment} from 'app/shared/model/department.model';
import {DepartmentService} from 'app/entities/department';
import {ILecturer} from 'app/shared/model/lecturer.model';
import {CoursesService} from 'app/entities/courses';
import {ICourses} from 'app/shared/model/courses.model';
import {ICase} from 'app/shared/model/scale.model';
import {IScores, Scores} from 'app/shared/model/scores.model';
import {LecturerService} from 'app/entities/lecturer';
import {QuestionService} from 'app/entities/question';
import {IQuestion, Question} from 'app/shared/model/question.model';
import {forEach} from "@angular/router/src/utils/collection";
import {IQuestionCategory, QuestionCategory} from 'app/shared/model/question-category.model';
import {QuestionCategoryService} from 'app/entities/question-category';

@Component({
    selector: 'jhi-assessment-update',
    templateUrl: './assessment-update.component.html'
})
export class AssessmentUpdateComponent implements OnInit {
    assessment: IAssessment;
    isSaving: boolean;
    faculties: IFaculty[];
    department: IDepartment;
    departments: IDepartment[];
    lecturers: ILecturer[];
    courses: ICourses[];
    public scored
    newVal: number;
    newVall: number;
    newValll: number;
    public sendData;
    questions: IQuestion[];
    public questionOne;
    public questionTwo;
    public questionThree;
    public questionFour;
    public questionFive;
    public details;
    public questionSix; public question7; public question8;
    questionCategory: IQuestionCategory[];
   // scales: ICase[];
    scales = [
        {
            id: 1,
            name: 'Very Poor'
        },
        {
            id: 2,
            name: 'poor'
        },
        {
            id: 3,
            name: 'Average'
        },
        {
            id: 4,
            name: 'Good'
        },
        {
            id: 5,
            name: 'Very Poor'
        }

    ];

    constructor(
        protected assessmentService: AssessmentService,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected facultyService: FacultyService,
        protected departmentService: DepartmentService,
        protected courseService: CoursesService,
        protected lecturerService: LecturerService,
        protected questionService: QuestionService,
        protected questiongCategoryService: QuestionCategoryService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ assessment }) => {
            this.assessment = assessment;

        });

        this.questiongCategoryService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IFaculty[]>) => mayBeOk.ok),
                map((response: HttpResponse<IFaculty[]>) => response.body)
            )
            .subscribe((res: IQuestionCategory[]) => (this.questionCategory = res), (res: HttpErrorResponse) => this.onError(res.message));



        this.loadData();

        this.facultyService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IFaculty[]>) => mayBeOk.ok),
                map((response: HttpResponse<IFaculty[]>) => response.body)
            )
            .subscribe((res: IFaculty[]) => (this.faculties = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }


    loadData() {

        this.questionOne = [];
        this.questionOne[0] = "";
        this.questionTwo = [];
        this.questionThree = [];
        this.questionFour = [];
        this.questionFive = [];
        this.questionSix = [];
        this.question7 = [];
        this.question8 = [];

       // this.questions = [];
        this.questionService
            .query()
            .pipe(
                filter((res: HttpResponse<IQuestion[]>) => res.ok),
                map((res: HttpResponse<IQuestion[]>) => res.body)
            )
            .subscribe(
                (res: IQuestion[]) => {
                    this.questions = res;
                   // console.log(this.questions.length);


                    console.log(this.questions.length);
                    for (let question of this.questions) {

                        console.log(question.questioncategoryId);
                        switch (question.questioncategoryId) {

                            case 1:
                                this.questionOne.push(question.content);
                                break;
                            case 2:
                                this.questionTwo.push(question.content);
                                break;
                            case 3:
                                this.questionThree.push(question.content);
                                break;
                            case 4:
                                this.questionFour.push(question.content);
                                break;
                            case 5:
                                this.questionFive.push(question.content);
                                break;
                            case 6:
                                this.questionSix.push(question.content);
                                break;
                            case 7:
                                this.question7.push(question.content);
                                break;
                            case 8:
                                this.question8.push(question.content);
                                break;
                        }

                    }

                    console.log(this.questionOne[0]);
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );






       // console.log(this.questionOne[0]);
    }
    save() {


        this.sendData = [];
        this.isSaving = true;
        if (this.assessment.id !== undefined) {
            this.subscribeToSaveResponse(this.assessmentService.update(this.assessment));
        } else {
           const shore = Number(this.assessment.sub) + Number(this.assessment.rel) + Number(this.assessment.example) + Number(this.assessment.add);
           const mop = Number(this.assessment.prep) + Number(this.assessment.approp);
           const cand = Number(this.assessment.enco) + Number(this.assessment.ident) + Number (this.assessment.express) + Number(this.assessment.resp) + Number(this.assessment.deal);
           const learn = Number(this.assessment.stim) + Number(this.assessment.acc);
           const approach = Number(this.assessment.open) + Number(this.assessment.treat);
           const general = Number(this.assessment.lect) + Number(this.assessment.regular) + Number (this.assessment.kept) + Number(this.assessment.scrip) + Number(this.assessment.overall);
           const envi = Number(this.assessment.seats) + Number(this.assessment.illum) + Number(this.assessment.vent + this.assessment.pas);
            console.log('Here it is' + general);
           this.details = [shore, mop, cand, learn, approach, general, envi];
           for (let bite = 0; bite < this.details.length; bite++ ){
               this.scored = new Scores();
               this.scored.courseId = Number(this.newVal);
               this.scored.questioncategoryId = bite + 1;
               this.scored.score = Number(this.details[bite]);
               this.scored.lecturerId = Number(this.assessment.lecturerId);
                this.sendData.push(this.scored);
               console.log(this.scored.toString());

           }
            this.subscribeToSaveResponse(this.assessmentService.send(this.sendData));

        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<any>>) {
        result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackFacultyById(index: number, item: IFaculty) {
        return item.id;
    }

    trackDepartmentById(index: number, item: IDepartment) {
        return item.id;
    }

    onChange(event) {
         this.newVall = event.target.value;
        console.log(this.newVall);

        this.departmentService
            .take(this.newVall)
            .pipe(
                filter((mayBeOk: HttpResponse<IDepartment[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDepartment[]>) => response.body)
            )
            .subscribe((res: IDepartment[]) => (this.departments = res), (res: HttpErrorResponse) => this.onError(res.message));

    }

    onChangeDepartment(event) {
        this.newVal = event.target.value;
        console.log(this.newVal);

        this.courseService
            .take(this.newVal)
            .pipe(
                filter((mayBeOk: HttpResponse<ICourses[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICourses[]>) => response.body)
            )
            .subscribe((res: ICourses[]) => (this.courses = res), (res: HttpErrorResponse) => this.onError(res.message));

    }

    onChangeCourse(event) {
        this.newValll = event.target.value;

        this.lecturerService
            .take(this.newValll)
            .pipe(
                filter((mayBeOk: HttpResponse<ILecturer[]>) => mayBeOk.ok),
                map((response: HttpResponse<ILecturer[]>) => response.body)
            )
            .subscribe((res: ILecturer[]) => (this.lecturers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

}
