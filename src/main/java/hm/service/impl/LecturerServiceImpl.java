package hm.service.impl;

import hm.service.LecturerService;
import hm.domain.Lecturer;
import hm.repository.LecturerRepository;
import hm.service.dto.LecturerDTO;
import hm.service.mapper.LecturerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Lecturer.
 */
@Service
@Transactional
public class LecturerServiceImpl implements LecturerService {

    private final Logger log = LoggerFactory.getLogger(LecturerServiceImpl.class);

    private final LecturerRepository lecturerRepository;

    private final LecturerMapper lecturerMapper;

    public LecturerServiceImpl(LecturerRepository lecturerRepository, LecturerMapper lecturerMapper) {
        this.lecturerRepository = lecturerRepository;
        this.lecturerMapper = lecturerMapper;
    }

    /**
     * Save a lecturer.
     *
     * @param lecturerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LecturerDTO save(LecturerDTO lecturerDTO) {
        log.debug("Request to save Lecturer : {}", lecturerDTO);
        Lecturer lecturer = lecturerMapper.toEntity(lecturerDTO);
        lecturer = lecturerRepository.save(lecturer);
        return lecturerMapper.toDto(lecturer);
    }

    /**
     * Get all the lecturers.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LecturerDTO> findAll() {
        log.debug("Request to get all Lecturers");
        return lecturerRepository.findAllWithEagerRelationships().stream()
            .map(lecturerMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the Lecturer with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<LecturerDTO> findAllWithEagerRelationships(Pageable pageable) {
        return lecturerRepository.findAllWithEagerRelationships(pageable).map(lecturerMapper::toDto);
    }
    

    /**
     * Get one lecturer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LecturerDTO> findOne(Long id) {
        log.debug("Request to get Lecturer : {}", id);
        return lecturerRepository.findOneWithEagerRelationships(id)
            .map(lecturerMapper::toDto);
    }

    /**
     * Delete the lecturer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Lecturer : {}", id);        lecturerRepository.deleteById(id);
    }

    @Override
    public List<Lecturer> findByDeparment(Long id) {
        return lecturerRepository.findAllByDepartmentId(id);
    }

    @Override
    public List<Lecturer> findByCourse(Long id) {
        return lecturerRepository.findAllByCoursesId(id);
    }


}
