package hm.service.impl;

import hm.service.ScoresService;
import hm.domain.Scores;
import hm.repository.ScoresRepository;
import hm.service.dto.ScoresDTO;
import hm.service.mapper.ScoresMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Scores.
 */
@Service
@Transactional
public class ScoresServiceImpl implements ScoresService {

    private final Logger log = LoggerFactory.getLogger(ScoresServiceImpl.class);

    private final ScoresRepository scoresRepository;

    private final ScoresMapper scoresMapper;

    public ScoresServiceImpl(ScoresRepository scoresRepository, ScoresMapper scoresMapper) {
        this.scoresRepository = scoresRepository;
        this.scoresMapper = scoresMapper;
    }

    /**
     * Save a scores.
     *
     * @param scoresDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ScoresDTO save(ScoresDTO scoresDTO) {
        log.debug("Request to save Scores : {}", scoresDTO);
        Scores scores = scoresMapper.toEntity(scoresDTO);
        scores = scoresRepository.save(scores);
        return scoresMapper.toDto(scores);
    }

    /**
     * Get all the scores.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ScoresDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Scores");
        return scoresRepository.findAll(pageable)
            .map(scoresMapper::toDto);
    }


    /**
     * Get one scores by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ScoresDTO> findOne(Long id) {
        log.debug("Request to get Scores : {}", id);
        return scoresRepository.findById(id)
            .map(scoresMapper::toDto);
    }

    /**
     * Delete the scores by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Scores : {}", id);        scoresRepository.deleteById(id);
    }

    @Override
    public List<ScoresDTO> saveAll(List<ScoresDTO> scoresDTOList) {
        List<Scores> scores = scoresMapper.toEntity(scoresDTOList);
        scores = scoresRepository.saveAll(scores);
        return scoresMapper.toDto(scores);
    }
}
