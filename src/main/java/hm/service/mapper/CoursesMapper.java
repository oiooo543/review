package hm.service.mapper;

import hm.domain.*;
import hm.service.dto.CoursesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Courses and its DTO CoursesDTO.
 */
@Mapper(componentModel = "spring", uses = {DepartmentMapper.class})
public interface CoursesMapper extends EntityMapper<CoursesDTO, Courses> {

    @Mapping(source = "department.id", target = "departmentId")
    CoursesDTO toDto(Courses courses);

    @Mapping(target = "scores", ignore = true)
    @Mapping(source = "departmentId", target = "department")
    @Mapping(target = "lecturers", ignore = true)
    Courses toEntity(CoursesDTO coursesDTO);

    default Courses fromId(Long id) {
        if (id == null) {
            return null;
        }
        Courses courses = new Courses();
        courses.setId(id);
        return courses;
    }
}
