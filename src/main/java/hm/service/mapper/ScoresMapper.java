package hm.service.mapper;

import hm.domain.*;
import hm.service.dto.ScoresDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Scores and its DTO ScoresDTO.
 */
@Mapper(componentModel = "spring", uses = {LecturerMapper.class, CoursesMapper.class, QuestionCategoryMapper.class})
public interface ScoresMapper extends EntityMapper<ScoresDTO, Scores> {

    @Mapping(source = "lecturer.id", target = "lecturerId")
    @Mapping(source = "course.id", target = "courseId")
    @Mapping(source = "questioncategory.id", target = "questioncategoryId")
    ScoresDTO toDto(Scores scores);

    @Mapping(source = "lecturerId", target = "lecturer")
    @Mapping(source = "courseId", target = "course")
    @Mapping(source = "questioncategoryId", target = "questioncategory")
    Scores toEntity(ScoresDTO scoresDTO);

    default Scores fromId(Long id) {
        if (id == null) {
            return null;
        }
        Scores scores = new Scores();
        scores.setId(id);
        return scores;
    }
}
