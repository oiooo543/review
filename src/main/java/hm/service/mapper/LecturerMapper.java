package hm.service.mapper;

import hm.domain.*;
import hm.service.dto.LecturerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Lecturer and its DTO LecturerDTO.
 */
@Mapper(componentModel = "spring", uses = {CoursesMapper.class, DepartmentMapper.class})
public interface LecturerMapper extends EntityMapper<LecturerDTO, Lecturer> {

    @Mapping(source = "department.id", target = "departmentId")
    LecturerDTO toDto(Lecturer lecturer);

    @Mapping(target = "scores", ignore = true)
    @Mapping(source = "departmentId", target = "department")
    Lecturer toEntity(LecturerDTO lecturerDTO);

    default Lecturer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Lecturer lecturer = new Lecturer();
        lecturer.setId(id);
        return lecturer;
    }
}
