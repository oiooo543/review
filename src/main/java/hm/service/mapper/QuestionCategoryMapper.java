package hm.service.mapper;

import hm.domain.*;
import hm.service.dto.QuestionCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity QuestionCategory and its DTO QuestionCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QuestionCategoryMapper extends EntityMapper<QuestionCategoryDTO, QuestionCategory> {


    @Mapping(target = "questions", ignore = true)
    @Mapping(target = "scores", ignore = true)
    QuestionCategory toEntity(QuestionCategoryDTO questionCategoryDTO);

    default QuestionCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        QuestionCategory questionCategory = new QuestionCategory();
        questionCategory.setId(id);
        return questionCategory;
    }
}
