package hm.service.mapper;

import hm.domain.*;
import hm.service.dto.QuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Question and its DTO QuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {QuestionCategoryMapper.class})
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {

    @Mapping(source = "questioncategory.id", target = "questioncategoryId")
    QuestionDTO toDto(Question question);

    @Mapping(source = "questioncategoryId", target = "questioncategory")
    Question toEntity(QuestionDTO questionDTO);

    default Question fromId(Long id) {
        if (id == null) {
            return null;
        }
        Question question = new Question();
        question.setId(id);
        return question;
    }
}
