package hm.service;

import hm.domain.Lecturer;
import hm.service.dto.LecturerDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Lecturer.
 */
public interface LecturerService {

    /**
     * Save a lecturer.
     *
     * @param lecturerDTO the entity to save
     * @return the persisted entity
     */
    LecturerDTO save(LecturerDTO lecturerDTO);

    /**
     * Get all the lecturers.
     *
     * @return the list of entities
     */
    List<LecturerDTO> findAll();

    /**
     * Get all the Lecturer with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<LecturerDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" lecturer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<LecturerDTO> findOne(Long id);

    /**
     * Delete the "id" lecturer.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<Lecturer> findByDeparment(Long id);

    List<Lecturer> findByCourse(Long id);
}
