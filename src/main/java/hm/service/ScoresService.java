package hm.service;

import hm.service.dto.ScoresDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Scores.
 */
public interface ScoresService {

    /**
     * Save a scores.
     *
     * @param scoresDTO the entity to save
     * @return the persisted entity
     */
    ScoresDTO save(ScoresDTO scoresDTO);

    /**
     * Get all the scores.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ScoresDTO> findAll(Pageable pageable);


    /**
     * Get the "id" scores.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ScoresDTO> findOne(Long id);

    /**
     * Delete the "id" scores.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<ScoresDTO> saveAll(List<ScoresDTO> scoresDTOList);
}
