package hm.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Courses entity.
 */
public class CoursesDTO implements Serializable {

    private Long id;

    private String title;

    private String code;


    private Long departmentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CoursesDTO coursesDTO = (CoursesDTO) o;
        if (coursesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), coursesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CoursesDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", code='" + getCode() + "'" +
            ", department=" + getDepartmentId() +
            "}";
    }
}
