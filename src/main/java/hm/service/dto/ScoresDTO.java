package hm.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Scores entity.
 */
public class ScoresDTO implements Serializable {

    private Long id;

    private Integer score;


    private Long lecturerId;

    private Long courseId;

    private Long questioncategoryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(Long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long coursesId) {
        this.courseId = coursesId;
    }

    public Long getQuestioncategoryId() {
        return questioncategoryId;
    }

    public void setQuestioncategoryId(Long questionCategoryId) {
        this.questioncategoryId = questionCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScoresDTO scoresDTO = (ScoresDTO) o;
        if (scoresDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scoresDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ScoresDTO{" +
            "id=" + getId() +
            ", score=" + getScore() +
            ", lecturer=" + getLecturerId() +
            ", course=" + getCourseId() +
            ", questioncategory=" + getQuestioncategoryId() +
            "}";
    }
}
