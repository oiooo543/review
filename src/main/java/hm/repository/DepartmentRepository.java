package hm.repository;

import hm.domain.Department;
import hm.service.dto.DepartmentDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Department entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    List<Department> findAllByFacultyId(Long id);

}
