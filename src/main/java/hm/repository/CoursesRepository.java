package hm.repository;

import hm.domain.Courses;
import hm.service.dto.CoursesDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Courses entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoursesRepository extends JpaRepository<Courses, Long> {

    List<Courses> findAllByDepartmentId(Long id);
}
