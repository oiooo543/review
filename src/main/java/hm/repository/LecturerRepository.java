package hm.repository;

import hm.domain.Lecturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Lecturer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LecturerRepository extends JpaRepository<Lecturer, Long> {

    @Query(value = "select distinct lecturer from Lecturer lecturer left join fetch lecturer.courses",
        countQuery = "select count(distinct lecturer) from Lecturer lecturer")
    Page<Lecturer> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct lecturer from Lecturer lecturer left join fetch lecturer.courses")
    List<Lecturer> findAllWithEagerRelationships();

    @Query("select lecturer from Lecturer lecturer left join fetch lecturer.courses where lecturer.id =:id")
    Optional<Lecturer> findOneWithEagerRelationships(@Param("id") Long id);


    List<Lecturer> findAllByDepartmentId(Long id);

    List<Lecturer> findAllByCoursesId(Long id);



}
