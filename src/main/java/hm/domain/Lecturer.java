package hm.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Lecturer.
 */
@Entity
@Table(name = "lecturer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Lecturer implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "lecturer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Scores> scores = new HashSet<>();
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "lecturer_courses",
               joinColumns = @JoinColumn(name = "lecturer_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "courses_id", referencedColumnName = "id"))
    private Set<Courses> courses = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("lecturers")
    private Department department;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Lecturer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Scores> getScores() {
        return scores;
    }

    public Lecturer scores(Set<Scores> scores) {
        this.scores = scores;
        return this;
    }

    public Lecturer addScores(Scores scores) {
        this.scores.add(scores);
        scores.setLecturer(this);
        return this;
    }

    public Lecturer removeScores(Scores scores) {
        this.scores.remove(scores);
        scores.setLecturer(null);
        return this;
    }

    public void setScores(Set<Scores> scores) {
        this.scores = scores;
    }

    public Set<Courses> getCourses() {
        return courses;
    }

    public Lecturer courses(Set<Courses> courses) {
        this.courses = courses;
        return this;
    }

    public Lecturer addCourses(Courses courses) {
        this.courses.add(courses);
        courses.getLecturers().add(this);
        return this;
    }

    public Lecturer removeCourses(Courses courses) {
        this.courses.remove(courses);
        courses.getLecturers().remove(this);
        return this;
    }

    public void setCourses(Set<Courses> courses) {
        this.courses = courses;
    }

    public Department getDepartment() {
        return department;
    }

    public Lecturer department(Department department) {
        this.department = department;
        return this;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lecturer lecturer = (Lecturer) o;
        if (lecturer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lecturer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Lecturer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
