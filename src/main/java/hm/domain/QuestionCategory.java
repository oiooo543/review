package hm.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A QuestionCategory.
 */
@Entity
@Table(name = "question_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QuestionCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "questioncategory")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Question> questions = new HashSet<>();
    @OneToMany(mappedBy = "questioncategory")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Scores> scores = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public QuestionCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public QuestionCategory questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public QuestionCategory addQuestion(Question question) {
        this.questions.add(question);
        question.setQuestioncategory(this);
        return this;
    }

    public QuestionCategory removeQuestion(Question question) {
        this.questions.remove(question);
        question.setQuestioncategory(null);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<Scores> getScores() {
        return scores;
    }

    public QuestionCategory scores(Set<Scores> scores) {
        this.scores = scores;
        return this;
    }

    public QuestionCategory addScores(Scores scores) {
        this.scores.add(scores);
        scores.setQuestioncategory(this);
        return this;
    }

    public QuestionCategory removeScores(Scores scores) {
        this.scores.remove(scores);
        scores.setQuestioncategory(null);
        return this;
    }

    public void setScores(Set<Scores> scores) {
        this.scores = scores;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QuestionCategory questionCategory = (QuestionCategory) o;
        if (questionCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), questionCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QuestionCategory{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
