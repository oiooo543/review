package hm.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Courses.
 */
@Entity
@Table(name = "courses")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Courses implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "course")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Scores> scores = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("courses")
    private Department department;

    @ManyToMany(mappedBy = "courses")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Lecturer> lecturers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Courses title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public Courses code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Scores> getScores() {
        return scores;
    }

    public Courses scores(Set<Scores> scores) {
        this.scores = scores;
        return this;
    }

    public Courses addScores(Scores scores) {
        this.scores.add(scores);
        scores.setCourse(this);
        return this;
    }

    public Courses removeScores(Scores scores) {
        this.scores.remove(scores);
        scores.setCourse(null);
        return this;
    }

    public void setScores(Set<Scores> scores) {
        this.scores = scores;
    }

    public Department getDepartment() {
        return department;
    }

    public Courses department(Department department) {
        this.department = department;
        return this;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<Lecturer> getLecturers() {
        return lecturers;
    }

    public Courses lecturers(Set<Lecturer> lecturers) {
        this.lecturers = lecturers;
        return this;
    }

    public Courses addLecturer(Lecturer lecturer) {
        this.lecturers.add(lecturer);
        lecturer.getCourses().add(this);
        return this;
    }

    public Courses removeLecturer(Lecturer lecturer) {
        this.lecturers.remove(lecturer);
        lecturer.getCourses().remove(this);
        return this;
    }

    public void setLecturers(Set<Lecturer> lecturers) {
        this.lecturers = lecturers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Courses courses = (Courses) o;
        if (courses.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), courses.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Courses{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
