package hm.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Department.
 */
@Entity
@Table(name = "department")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "department")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Student> students = new HashSet<>();
    @OneToMany(mappedBy = "department")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Courses> courses = new HashSet<>();
    @OneToMany(mappedBy = "department")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Lecturer> lecturers = new HashSet<>();
    @OneToMany(mappedBy = "department")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Program> programs = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("departments")
    private Faculty faculty;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Department name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Department students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public Department addStudent(Student student) {
        this.students.add(student);
        student.setDepartment(this);
        return this;
    }

    public Department removeStudent(Student student) {
        this.students.remove(student);
        student.setDepartment(null);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Courses> getCourses() {
        return courses;
    }

    public Department courses(Set<Courses> courses) {
        this.courses = courses;
        return this;
    }

    public Department addCourses(Courses courses) {
        this.courses.add(courses);
        courses.setDepartment(this);
        return this;
    }

    public Department removeCourses(Courses courses) {
        this.courses.remove(courses);
        courses.setDepartment(null);
        return this;
    }

    public void setCourses(Set<Courses> courses) {
        this.courses = courses;
    }

    public Set<Lecturer> getLecturers() {
        return lecturers;
    }

    public Department lecturers(Set<Lecturer> lecturers) {
        this.lecturers = lecturers;
        return this;
    }

    public Department addLecturer(Lecturer lecturer) {
        this.lecturers.add(lecturer);
        lecturer.setDepartment(this);
        return this;
    }

    public Department removeLecturer(Lecturer lecturer) {
        this.lecturers.remove(lecturer);
        lecturer.setDepartment(null);
        return this;
    }

    public void setLecturers(Set<Lecturer> lecturers) {
        this.lecturers = lecturers;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public Department programs(Set<Program> programs) {
        this.programs = programs;
        return this;
    }

    public Department addProgram(Program program) {
        this.programs.add(program);
        program.setDepartment(this);
        return this;
    }

    public Department removeProgram(Program program) {
        this.programs.remove(program);
        program.setDepartment(null);
        return this;
    }

    public void setPrograms(Set<Program> programs) {
        this.programs = programs;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public Department faculty(Faculty faculty) {
        this.faculty = faculty;
        return this;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Department department = (Department) o;
        if (department.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), department.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Department{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
