package hm.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Scores.
 */
@Entity
@Table(name = "scores")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Scores implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "score")
    private Integer score;

    @ManyToOne
    @JsonIgnoreProperties("scores")
    private Lecturer lecturer;

    @ManyToOne
    @JsonIgnoreProperties("scores")
    private Courses course;

    @ManyToOne
    @JsonIgnoreProperties("scores")
    private QuestionCategory questioncategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public Scores score(Integer score) {
        this.score = score;
        return this;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public Scores lecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
        return this;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Courses getCourse() {
        return course;
    }

    public Scores course(Courses courses) {
        this.course = courses;
        return this;
    }

    public void setCourse(Courses courses) {
        this.course = courses;
    }

    public QuestionCategory getQuestioncategory() {
        return questioncategory;
    }

    public Scores questioncategory(QuestionCategory questionCategory) {
        this.questioncategory = questionCategory;
        return this;
    }

    public void setQuestioncategory(QuestionCategory questionCategory) {
        this.questioncategory = questionCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Scores scores = (Scores) o;
        if (scores.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scores.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Scores{" +
            "id=" + getId() +
            ", score=" + getScore() +
            "}";
    }
}
