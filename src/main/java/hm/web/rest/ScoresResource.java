package hm.web.rest;
import hm.service.ScoresService;
import hm.web.rest.errors.BadRequestAlertException;
import hm.web.rest.util.HeaderUtil;
import hm.web.rest.util.PaginationUtil;
import hm.service.dto.ScoresDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Scores.
 */
@RestController
@RequestMapping("/api")
public class ScoresResource {

    private final Logger log = LoggerFactory.getLogger(ScoresResource.class);

    private static final String ENTITY_NAME = "scores";

    private final ScoresService scoresService;

    public ScoresResource(ScoresService scoresService) {
        this.scoresService = scoresService;
    }

    /**
     * POST  /scores : Create a new scores.
     *
     * @param scoresDTO the scoresDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new scoresDTO, or with status 400 (Bad Request) if the scores has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/scores")
    public ResponseEntity<ScoresDTO> createScores(@RequestBody ScoresDTO scoresDTO) throws URISyntaxException {
        log.debug("REST request to save Scores : {}", scoresDTO);
        if (scoresDTO.getId() != null) {
            throw new BadRequestAlertException("A new scores cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ScoresDTO result = scoresService.save(scoresDTO);
        return ResponseEntity.created(new URI("/api/scores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /scores : Updates an existing scores.
     *
     * @param scoresDTO the scoresDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated scoresDTO,
     * or with status 400 (Bad Request) if the scoresDTO is not valid,
     * or with status 500 (Internal Server Error) if the scoresDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/scores")
    public ResponseEntity<ScoresDTO> updateScores(@RequestBody ScoresDTO scoresDTO) throws URISyntaxException {
        log.debug("REST request to update Scores : {}", scoresDTO);
        if (scoresDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ScoresDTO result = scoresService.save(scoresDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, scoresDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /scores : get all the scores.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of scores in body
     */
    @GetMapping("/scores")
    public ResponseEntity<List<ScoresDTO>> getAllScores(Pageable pageable) {
        log.debug("REST request to get a page of Scores");
        Page<ScoresDTO> page = scoresService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/scores");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /scores/:id : get the "id" scores.
     *
     * @param id the id of the scoresDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the scoresDTO, or with status 404 (Not Found)
     */
    @GetMapping("/scores/{id}")
    public ResponseEntity<ScoresDTO> getScores(@PathVariable Long id) {
        log.debug("REST request to get Scores : {}", id);
        Optional<ScoresDTO> scoresDTO = scoresService.findOne(id);
        return ResponseUtil.wrapOrNotFound(scoresDTO);
    }

    /**
     * DELETE  /scores/:id : delete the "id" scores.
     *
     * @param id the id of the scoresDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/scores/{id}")
    public ResponseEntity<Void> deleteScores(@PathVariable Long id) {
        log.debug("REST request to delete Scores : {}", id);
        scoresService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/scoreslist")
    public ResponseEntity<ScoresDTO> createScores(@RequestBody List<ScoresDTO> scoresDTO) throws URISyntaxException {
        log.debug("REST request to save Scores : {}", scoresDTO);

       List <ScoresDTO> result = scoresService.saveAll(scoresDTO);
        return ResponseEntity.ok()
            .body(result.get(0));
    }


}
