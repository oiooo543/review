/**
 * View Models used by Spring MVC REST controllers.
 */
package hm.web.rest.vm;
