package hm.web.rest;
import hm.domain.Courses;
import hm.service.CoursesService;
import hm.web.rest.errors.BadRequestAlertException;
import hm.web.rest.util.HeaderUtil;
import hm.web.rest.util.PaginationUtil;
import hm.service.dto.CoursesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Courses.
 */
@RestController
@RequestMapping("/api")
public class CoursesResource {

    private final Logger log = LoggerFactory.getLogger(CoursesResource.class);

    private static final String ENTITY_NAME = "courses";

    private final CoursesService coursesService;

    public CoursesResource(CoursesService coursesService) {
        this.coursesService = coursesService;
    }

    /**
     * POST  /courses : Create a new courses.
     *
     * @param coursesDTO the coursesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new coursesDTO, or with status 400 (Bad Request) if the courses has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/courses")
    public ResponseEntity<CoursesDTO> createCourses(@RequestBody CoursesDTO coursesDTO) throws URISyntaxException {
        log.debug("REST request to save Courses : {}", coursesDTO);
        if (coursesDTO.getId() != null) {
            throw new BadRequestAlertException("A new courses cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoursesDTO result = coursesService.save(coursesDTO);
        return ResponseEntity.created(new URI("/api/courses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /courses : Updates an existing courses.
     *
     * @param coursesDTO the coursesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated coursesDTO,
     * or with status 400 (Bad Request) if the coursesDTO is not valid,
     * or with status 500 (Internal Server Error) if the coursesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/courses")
    public ResponseEntity<CoursesDTO> updateCourses(@RequestBody CoursesDTO coursesDTO) throws URISyntaxException {
        log.debug("REST request to update Courses : {}", coursesDTO);
        if (coursesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CoursesDTO result = coursesService.save(coursesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, coursesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /courses : get all the courses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of courses in body
     */
    @GetMapping("/courses")
    public ResponseEntity<List<CoursesDTO>> getAllCourses(Pageable pageable) {
        log.debug("REST request to get a page of Courses");
        Page<CoursesDTO> page = coursesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/courses");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /courses/:id : get the "id" courses.
     *
     * @param id the id of the coursesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the coursesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/courses/{id}")
    public ResponseEntity<CoursesDTO> getCourses(@PathVariable Long id) {
        log.debug("REST request to get Courses : {}", id);
        Optional<CoursesDTO> coursesDTO = coursesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coursesDTO);
    }

    /**
     * DELETE  /courses/:id : delete the "id" courses.
     *
     * @param id the id of the coursesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/courses/{id}")
    public ResponseEntity<Void> deleteCourses(@PathVariable Long id) {
        log.debug("REST request to delete Courses : {}", id);
        coursesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/departmentcourses/{id}")
    public List<Courses> courseByDepartment(@PathVariable Long id){
      return  coursesService.findByDepartment(id);
    }
}
