package hm.web.rest;
import hm.domain.Lecturer;
import hm.service.LecturerService;
import hm.web.rest.errors.BadRequestAlertException;
import hm.web.rest.util.HeaderUtil;
import hm.service.dto.LecturerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Lecturer.
 */
@RestController
@RequestMapping("/api")
public class LecturerResource {

    private final Logger log = LoggerFactory.getLogger(LecturerResource.class);

    private static final String ENTITY_NAME = "lecturer";

    private final LecturerService lecturerService;

    public LecturerResource(LecturerService lecturerService) {
        this.lecturerService = lecturerService;
    }

    /**
     * POST  /lecturers : Create a new lecturer.
     *
     * @param lecturerDTO the lecturerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lecturerDTO, or with status 400 (Bad Request) if the lecturer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/lecturers")
    public ResponseEntity<LecturerDTO> createLecturer(@RequestBody LecturerDTO lecturerDTO) throws URISyntaxException {
        log.debug("REST request to save Lecturer : {}", lecturerDTO);
        if (lecturerDTO.getId() != null) {
            throw new BadRequestAlertException("A new lecturer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LecturerDTO result = lecturerService.save(lecturerDTO);
        return ResponseEntity.created(new URI("/api/lecturers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /lecturers : Updates an existing lecturer.
     *
     * @param lecturerDTO the lecturerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lecturerDTO,
     * or with status 400 (Bad Request) if the lecturerDTO is not valid,
     * or with status 500 (Internal Server Error) if the lecturerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/lecturers")
    public ResponseEntity<LecturerDTO> updateLecturer(@RequestBody LecturerDTO lecturerDTO) throws URISyntaxException {
        log.debug("REST request to update Lecturer : {}", lecturerDTO);
        if (lecturerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LecturerDTO result = lecturerService.save(lecturerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lecturerDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /lecturers : get all the lecturers.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of lecturers in body
     */
    @GetMapping("/lecturers")
    public List<LecturerDTO> getAllLecturers(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Lecturers");
        return lecturerService.findAll();
    }

    /**
     * GET  /lecturers/:id : get the "id" lecturer.
     *
     * @param id the id of the lecturerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lecturerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/lecturers/{id}")
    public ResponseEntity<LecturerDTO> getLecturer(@PathVariable Long id) {
        log.debug("REST request to get Lecturer : {}", id);
        Optional<LecturerDTO> lecturerDTO = lecturerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(lecturerDTO);
    }

    /**
     * DELETE  /lecturers/:id : delete the "id" lecturer.
     *
     * @param id the id of the lecturerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/lecturers/{id}")
    public ResponseEntity<Void> deleteLecturer(@PathVariable Long id) {
        log.debug("REST request to delete Lecturer : {}", id);
        lecturerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/departmentLecturer/{id}")
    public List<Lecturer> findByDepartment(@PathVariable Long id){
        return lecturerService.findByDeparment(id);
    }

    @GetMapping("/courselecturer/{id}")
    public List<Lecturer> findByCourse(@PathVariable Long id) {
        return lecturerService.findByCourse(id);
    }
}
